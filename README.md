# Laravel Rest API

> Este repositório consiste em uma API Rest desenvolvida em Laravel 9 para fins de teste de conhecimento.

## Instalação

- A inicialização do projeto foi realizada através do sail no Laravel, definindo as configurações ambiente docker padrões para trabalho.
    > ./vendor/bin/sail up
- É necessário rodar a `migration` principal que, por sua vez, irá executar as demais. Juntamente com a `migration` é possível rodar a `seed` que utiliza `model factories` para gerar registros fictícios. Obs.: Necessário executar no container do `sail`
    > php artisan migrate:fresh --seed

## Utilização
- Por fim a API fica disponível a partir da rota: http://localhost:5001/api


[Laravel 9](https://laravel.com/docs/9.x)
