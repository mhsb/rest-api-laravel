<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use Illuminate\Http\Request;
use App\Http\Requests\ClienteRequest;
use Illuminate\Support\Facades\App;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clientes = Cliente::where('nome', 'like', '%'. $request->search .'%')
                        ->orWhere('email', 'like', '%'. $request->search .'%')
                        ->paginate(10);
        
        return response($clientes, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ClienteRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteRequest $request)
    {
        $cliente = new Cliente([
            'nome'     => $request->nome,
            'email'    => $request->email,
            'endereco' => $request->endereco,
            'telefone' => $request->telefone
        ]);
        
        $saved = $cliente->save();
        
        if (! $saved) {
            return response()->json([
                "message" => "Erro interno, registro não criado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Registro de cliente criado com sucesso!",
            "id" => $cliente->id
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::find($id);
        
        if (! $cliente) {
            return response()->json([
                "message" => "Cliente não encontrado."
            ], 404);
        }
        
        return response($cliente, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ClienteRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteRequest $request, $id)
    {
        $cliente = Cliente::find($id);
        
        if (! $cliente) {
            return response()->json([
                "message" => "Cliente não encontrado."
            ], 404);
        }
        
        $cliente->nome     = $request->nome;
        $cliente->email    = $request->email;
        $cliente->endereco = $request->endereco;
        $cliente->telefone = $request->telefone;
        
        $saved = $cliente->save();
        
        if (! $saved) {
            return response()->json([
                "message" => "Erro interno, registro não atualizado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Cliente atualizado com sucesso!"
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::find($id);
        
        if (! $cliente) {
            return response()->json([
                "message" => "Cliente não encontrado."
            ], 404);
        }
        
        $deleted = $cliente->destroy($id);
        
        if (! $deleted) {
            return response()->json([
                "message" => "Erro interno, registro não deletado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Cliente deletado com sucesso!"
        ], 200);
    }
}
