<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FornecedorRequest;
use App\Models\Fornecedor;
use Illuminate\Http\Request;

class FornecedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fornecedores = Fornecedor::where('razao_social', 'like', '%'. $request->search .'%')
                                ->orWhere('email', 'like', '%'. $request->search .'%')
                                ->paginate(10);

        return response($fornecedores, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\FornecedorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FornecedorRequest $request)
    {
        $fornecedor = new Fornecedor([
            'razao_social' => $request->razao_social,
            'email'        => $request->email,
            'endereco'     => $request->endereco,
            'telefone'     => $request->telefone
        ]);
        
        $saved = $fornecedor->save();
        
        if (! $saved) {
            return response()->json([
                "message" => "Erro interno, registro não criado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Registro de fornecedor criado com sucesso!",
            "id" => $fornecedor->id
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fornecedor = Fornecedor::find($id);
        
        if (! $fornecedor) {
            return response()->json([
                "message" => "Fornecedor não encontrado."
            ], 404);
        }
        
        return response($fornecedor, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\FornecedorRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FornecedorRequest $request, $id)
    {
        $fornecedor = Fornecedor::find($id);
        
        if (! $fornecedor) {
            return response()->json([
                "message" => "Fornecedor não encontrado."
            ], 404);
        }
        
        $fornecedor->razao_social = $request->razao_social;
        $fornecedor->email        = $request->email;
        $fornecedor->endereco     = $request->endereco;
        $fornecedor->telefone     = $request->telefone;
        
        $saved = $fornecedor->save();
        
        if (! $saved) {
            return response()->json([
                "message" => "Erro interno, registro não atualizado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Fornecedor atualizado com sucesso!"
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) 
    {
        $fornecedor = Fornecedor::find($id);
        
        if (! $fornecedor) {
            return response()->json([
                "message" => "Fornecedor não encontrado."
            ], 404);
        }
        
        $deleted = $fornecedor->destroy($id);
        
        if (! $deleted) {
            return response()->json([
                "message" => "Erro interno, registro não deletado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Fornecedor deletado com sucesso!"
        ], 200);
    }
}
