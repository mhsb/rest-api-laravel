<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $produtos = Produto::with(['fornecedor'])
                        ->where('nome', 'like', '%'. $request->search .'%')
                        ->orWhere('descricao', 'like', '%'. $request->search .'%')
                        ->orWhereRelation('fornecedor', 'nome', 'like', '%'. $request->search .'%')
                        ->paginate(10);

        return response($produtos, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produto = new Produto([
            'nome'          => $request->nome,
            'descricao'     => $request->descricao,
            'valor'         => $request->valor,
            'fornecedor_id' => $request->fornecedor_id
        ]);
        
        $saved = $produto->save();
        
        if (! $saved) {
            return response()->json([
                "message" => "Erro interno, registro não criado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Registro de produto criado com sucesso!",
            "id" => $produto->id
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produto = Produto::with(['fornecedor'])->find($id);
        
        if (! $produto) {
            return response()->json([
                "message" => "Produto não encontrado."
            ], 404);
        }
        
        return response($produto, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produto = Produto::find($id);
        
        if (! $produto) {
            return response()->json([
                "message" => "Produto não encontrado."
            ], 404);
        }

        $produto->nome          = $request->nome;
        $produto->descricao     = $request->descricao;
        $produto->valor         = $request->valor;
        $produto->fornecedor_id = $request->fornecedor_id;
        
        $saved = $produto->save();
        
        if (! $saved) {
            return response()->json([
                "message" => "Erro interno, registro não atualizado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Produto atualizado com sucesso!"
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = Produto::find($id);
        
        if (! $produto) {
            return response()->json([
                "message" => "Produto não encontrado."
            ], 404);
        }
        
        $deleted = $produto->destroy($id);
        
        if (! $deleted) {
            return response()->json([
                "message" => "Erro interno, registro não deletado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Produto deletado com sucesso!"
        ], 200);
    }
}
