<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VendaRequest;
use App\Models\Fornecedor;
use App\Models\Venda;
use Illuminate\Http\Request;

class VendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $vendas = Venda::with(['cliente', 'produtos'])
                ->whereRelation('cliente', 'nome', 'like', '%'. $request->search .'%')
                ->orWhereRelation('cliente', 'email', 'like', '%'. $request->search .'%')
                ->orWhereRelation('produtos', 'nome', 'like', '%'. $request->search .'%')
                ->get();

        foreach ($vendas as $venda) {
            foreach ($venda->produtos as $produto) {
                $produto['fornecedor_nome'] = Fornecedor::find($produto->fornecedor_id)->razao_social;
            }
        }
        
        return response($vendas, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\VendaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendaRequest $request)
    {
        $venda = new Venda([
            'cliente_id'  => $request->cliente_id,
            'valor_total' => 0
        ]);
        
        $venda->save();
        
        foreach ($request->produtos as $produto) {
            $venda->produtos()->attach($produto['id'], ['quantidade' => $produto['quantidade']]);
        }
        
        $updated = $this->updateValorTotal($venda);
        
        if (! $updated) {
            $venda->destroy($venda->id);
            
            return response()->json([
                "message" => "Erro interno, registro não criado. Rollback de venda realizado!"
            ], 500);
        }
        
        return response()->json([
            "message" => "Registro de venda criado com sucesso!",
            "id" => $venda->id
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venda = Venda::with(['cliente', 'produtos'])->find($id);
        
        if (! $venda) {
            return response()->json([
                "message" => "Venda não encontrada."
            ], 404);
        }
        
        return response($venda, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\VendaRequest  $request
     * @param  int id
     * @return \Illuminate\Http\Response
     */
    public function update(VendaRequest $request, $id)
    {
        $venda = Venda::find($id);
        
        if (! $venda) {
            return response()->json([
                "message" => "Venda não encontrada."
            ], 404);
        }

        $venda->produtos()->detach();

        foreach ($request->produtos as $produto) {
            $venda->produtos()->attach($produto['id'], ['quantidade' => $produto['quantidade']]);
        }
        
        $updated = $this->updateValorTotal($venda);
        
        if (! $updated) {
            
            return response()->json([
                "message" => "Erro interno, registro não atualizado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Produto atualizado com sucesso!"
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venda = Venda::find($id);
        
        if (! $venda) {
            return response()->json([
                "message" => "Venda não encontrado."
            ], 404);
        }
        
        $deleted = $venda->destroy($id);
        
        if (! $deleted) {
            return response()->json([
                "message" => "Erro interno, registro não deletado."
            ], 500);
        }
        
        return response()->json([
            "message" => "Venda deletada com sucesso!"
        ], 200);
    }

    private function updateValorTotal(Venda $venda) 
    {
        $resultado = 0;
        
        foreach( $venda->produtos as $produto) {
            $resultado += $produto->valor;
        }
        
        $venda->valor_total = $resultado;
        
        $updated = $venda->save();

        return $updated;
    }
}
