<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FornecedorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razao_social' => ['required', 'min:3'],
            'email'        => ['required', 'email:rfc', 'unique:fornecedores,email,' . $this->fornecedor],
            'endereco'     => ['required', 'min:3'],
            'telefone'     => ['max:15']
        ];
    }
}
