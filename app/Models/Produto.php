<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    use HasFactory;
    protected $table = 'produtos';
    protected $fillable = ['nome', 'descricao', 'valor', 'fornecedor_id'];
    
    public function fornecedor() {
        return $this->hasOne(Fornecedor::class, 'id', 'fornecedor_id');
    }
}
