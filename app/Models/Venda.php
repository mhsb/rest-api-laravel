<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    use HasFactory;
    protected $table = 'vendas';
    protected $fillable = ['valor_total', 'cliente_id'];
    
    public function cliente()
    {
        return $this->hasOne(Cliente::class, 'id', 'cliente_id');
    }
    
    public function produtos() {
        return $this->belongsToMany(Produto::class, 'produto_venda')
                    ->withTimestamps()
                    ->withPivot(['quantidade']);
    }
}
