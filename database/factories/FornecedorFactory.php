<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Fornecedor>
 */
class FornecedorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'razao_social' => $this->faker->company(),
            'endereco'     => $this->faker->address(),
            'email'        => $this->faker->unique()->safeEmail(),
            'telefone'     => $this->faker->phoneNumber()
        ];
    }
}
