<?php

namespace Database\Factories;

use App\Models\Fornecedor;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Produto>
 */
class ProdutoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nome'          => $this->faker->sentence(3),
            'descricao'     => $this->faker->paragraph(),
            'valor'         => $this->faker->randomFloat(2, 0.01, 99999.99),
            'fornecedor_id' => Fornecedor::factory()->create()->id
        ];
    }
}
