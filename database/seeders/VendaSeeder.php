<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Venda;

class VendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Venda::factory(10)->create();

        foreach (Venda::all() as $venda) {
            $produtos = \App\Models\Produto::inRandomOrder()->take(rand(1, 10))->pluck('id');
            foreach ($produtos as $produto) {
                $venda->produtos()->attach($produto, ['quantidade' => rand(1, 20)]);
            }
        }
    }
}
